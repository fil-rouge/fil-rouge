
let toutesValeurs;

$("button").click(function() {
    toutesValeurs = frontvalue + backvalue + anglaisValue + partageValue;
    $("#fullstackprogress").attr("value", toutesValeurs);
});

/*Sons des boutons*/
$(document).ready(function () {
    var levelup = $("#levelup");
    var leveldown = $("#leveldown");

    $(".plus").click(function () {
        levelup.get(0).play();
    });
    $(".moins").click(function () {
        leveldown.get(0).play();
    });
});

/*Boutons et jauge FRONT*/
var maquettevalue = 0;

$("#front > ul > li > button.plus.ma").click(function () {
    if (maquettevalue < $("#maquette").attr("max")) {
        maquettevalue++;
        $("#maquette").attr("value", maquettevalue);
    }
});
$("#front > ul > li > button.moins.ma").click(function () {
    if (maquettevalue > $("#maquette").attr("min")) {
        maquettevalue--;
        $("#maquette").attr("value", maquettevalue);
    }
});


var statiquevalue = 0;

$("#front > ul > li > button.plus.st").click(function () {
    if (statiquevalue < $("#statique").attr("max")) {
        statiquevalue++;
        $("#statique").attr("value", statiquevalue);
    }
});

$("#front > ul > li > button.moins.st").click(function () {
    if (statiquevalue > $("#statique").attr("min")) {
        statiquevalue--;
        $("#statique").attr("value", statiquevalue);
    }
});

var dynamiquevalue = 0;

$("#front > ul > li > button.plus.dy").click(function () {
    if (dynamiquevalue < $("#dynamique").attr("max")) {
        dynamiquevalue++;
        $("#dynamique").attr("value", dynamiquevalue);
    }
});

$("#front > ul > li > button.moins.dy").click(function () {
    if (dynamiquevalue > $("#dynamique").attr("min")) {
        dynamiquevalue--;
        $("#dynamique").attr("value", dynamiquevalue);
    }
});


var realisercmsvalue = 0;
$("#front > ul > li > button.plus.cms").click(function () {
    if (realisercmsvalue < $("#realisercms").attr("max")) {
        realisercmsvalue++;
        $("#realisercms").attr("value", realisercmsvalue);
    }
});

$("#front > ul > li > button.moins.cms").click(function () {
    if (realisercmsvalue > $("#realisercms").attr("min")) {
        realisercmsvalue--;
        $("#realisercms").attr("value", realisercmsvalue);
    }
});

var frontvalue;

$("#front > ul > li > button.plus").click(function () {
    frontvalue = maquettevalue + statiquevalue + dynamiquevalue + realisercmsvalue;
    $("#frontprogress").attr("value", frontvalue);
});

$("#front > ul > li > button.moins").click(function () {
    frontvalue = maquettevalue + statiquevalue + dynamiquevalue + realisercmsvalue;
    $("#frontprogress").attr("value", frontvalue);
});

/*Boutons et jauge BACK*/

var basedonnees = 0;
$("#back > ul > li > button.plus.bdd").click(function () {
    if (basedonnees < $("#basededonnees").attr("max")) {
        basedonnees++;
        $("#basededonnees").attr("value", basedonnees);
    }
});
$("#back > ul > li > button.moins.bdd").click(function () {
    if (basedonnees > $("#basededonnees").attr("min")) {
        basedonnees--;
        $("#basededonnees").attr("value", basedonnees);
    }
});

var compdonnees = 0;
$("#back > ul > li > button.plus.accd").click(function () {
    if (compdonnees < $("#accesdonnees").attr("max")) {
        compdonnees++;
        $("#accesdonnees").attr("value", compdonnees);
    }
});
$("#back > ul > li > button.moins.accd").click(function () {
    if (compdonnees > $("#accesdonnees").attr("min")) {
        compdonnees--;
        $("#accesdonnees").attr("value", compdonnees);
    }
});

var devbackend = 0;
$("#back > ul > li > button.plus.be").click(function () {
    if (devbackend < $("#backend").attr("max")) {
        devbackend++;
        $("#backend").attr("value", devbackend);
    }
});
$("#back > ul > li > button.moins.be").click(function () {
    if (devbackend > $("#backend").attr("min")) {
        devbackend--;
        $("#backend").attr("value", devbackend);
    }
});

var compecomm = 0;
$("#back > ul > li > button.plus.ecomm").click(function () {
    if (compecomm < $("#ecomm").attr("max")) {
        compecomm++;
        $("#ecomm").attr("value", compecomm);
    }
});
$("#back > ul > li > button.moins.ecomm").click(function () {
    if (compecomm > $("#ecomm").attr("max")) {
        compecomm--;
        $("#ecomm").attr("value", compecomm);
    }
});


var backvalue = basedonnees + compdonnees + devbackend + compecomm;
$("#back > ul > li > button.plus").click(function () {
    if (backvalue < $("#backprogress").attr("max")) {
        backvalue++;
        $("#backprogress").attr("value", backvalue);
    }
});
$("#back > ul > li > button.moins").click(function () {
    if (backvalue > $("#backprogress").attr("min")) {
        backvalue--;
        $("#backprogress").attr("value", backvalue);
    }
});

    // Jauge "Anglais"
    var anglaisValue = 0;
    $("#anglais > button.plus").click(function() {
        if (anglaisValue < $("#anglais > progress").attr("max")) {
            anglaisValue++;
            $("#anglais > progress").attr("value", anglaisValue);
        }
    });
    $("#anglais > button.moins").click(function() {
        if (anglaisValue > 0) {
            anglaisValue--;
            $("#anglais > progress").attr("value", anglaisValue);
        }
    });

    // Jauge "Partage"
    var partageValue = 0;
    $("#partage > button.plus").click(function() {
        if (partageValue < $("#partage > progress").attr("max")) {
            partageValue++;
            $("#partage > progress").attr("value", partageValue);
        }
    });
    $("#partage > button.moins").click(function() {
        if (partageValue > 0) {
            partageValue--;
            $("#partage > progress").attr("value", partageValue);
        }
    });

    /*Jauge "Total"
    var leTotal;
    $("button").click(function() {
        leTotal = anglaisValue + partageValue;
        $("#total > progress").attr("value", leTotal);
    });
});*/

/*Outils*/

var htmlvalue = 0;

$("#picto > div > button.plus.h").click(function () {
    if (htmlvalue < $("#html").attr("max")) {
        htmlvalue++;
        $("#html").attr("value", htmlvalue);
    }
});

$("#picto > div > button.moins.h").click(function () {
    if (htmlvalue > $("#html").attr("min")) {
        htmlvalue--;
        $("#html").attr("value", htmlvalue);
    }
});

var cssvalue = 0;

$("#picto > div > button.plus.c").click(function () {
    if (cssvalue < $("#css").attr("max")) {
        cssvalue++;
        $("#css").attr("value", cssvalue);
    }
});

$("#picto > div > button.moins.c").click(function () {
    if (cssvalue > $("#css").attr("min")) {
        cssvalue--;
        $("#css").attr("value", cssvalue);
    }
});

var jsvalue = 0;

$("#picto > div > button.plus.j").click(function () {
    if (jsvalue < $("#js").attr("max")) {
        jsvalue++;
        $("#js").attr("value", jsvalue);
    }
});

$("#picto > div > button.moins.j").click(function () {
    if (jsvalue > $("#js").attr("min")) {
        jsvalue--;
        $("#js").attr("value", jsvalue);
    }
});

var jqueryvalue = 0;

$("#picto > div > button.plus.jq").click(function () {
    if (jqueryvalue < $("#jquery").attr("max")) {
        jqueryvalue++;
        $("#jquery").attr("value", jqueryvalue);
    }
});

$("#picto > div > button.moins.jq").click(function () {
    if (jqueryvalue > $("#jquery").attr("min")) {
        jqueryvalue--;
        $("#jquery").attr("value", jqueryvalue);
    }
});

var phpvalue = 0;

$("#picto > div > button.plus.php").click(function () {
    if (phpvalue < $("#php").attr("max")) {
        phpvalue++;
        $("#php").attr("value", phpvalue);
    }
});

$("#picto > div > button.moins.php").click(function () {
    if (phpvalue > $("#php").attr("min")) {
        phpvalue--;
        $("#php").attr("value", phpvalue);
    }
});

var laravelvalue = 0;

$("#picto > div > button.plus.la").click(function () {
    if (laravelvalue < $("#laravel").attr("max")) {
        laravelvalue++;
        $("#laravel").attr("value", laravelvalue);
    }
});

$("#picto > div > button.moins.la").click(function () {
    if (laravelvalue > $("#laravel").attr("min")) {
        laravelvalue--;
        $("#laravel").attr("value", laravelvalue);
    }
});

